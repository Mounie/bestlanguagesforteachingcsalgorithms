#!/usr/bin/raku

sub emptyline($a) {
    if ($a == "") {
	"|";
    }
    else {
	$a;
    };
}

my @lines = "input1".IO.lines().map: &emptyline;
my @li = @lines.join: " ";
my @lis = @li.split: "|";
for @lis <-> $a { $a = $a.split(" ") };
for @lis <-> $a { $a = $a.sum() };
say @lis.max;
my @liso = @lis.sort();
say @liso[*-1,*-2,*-3];
