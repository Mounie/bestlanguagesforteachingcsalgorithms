#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

enum MaxType { MAX, NIL };
// obtenir une ligne

struct MaxVal {
  enum MaxType mt;
  int max;
};

struct MaxVal get_somme_groupe(FILE *f) {
  struct MaxVal mv = {NIL, 0};
  if (feof(f))
    return mv;

  while (true) {
    char *lineptr = NULL;
    size_t n = 0;
    getline(&lineptr, &n, f);
    if (lineptr == NULL || n == 0)
      break;

    int val = 0;
    int nbscan = sscanf(lineptr, " %d", &val);
    free(lineptr);

    if (nbscan == -1)
      break;

    mv.mt = MAX;
    mv.max += val;

    if (feof(f))
      break;
  }
  return mv;
}

struct MaxVal get_max(FILE *f) {
  struct MaxVal mv = {NIL, 0};

  struct MaxVal t = get_somme_groupe(f);
  while (t.mt != NIL) {
    if (mv.mt == NIL || mv.max < t.max)
      mv = t;
    t = get_somme_groupe(f);
  }
  return mv;
}

static int cmpmaxval(const void *p1, const void *p2) {
  return ((struct MaxVal *)p2)->max - ((struct MaxVal *)p1)->max;
}

struct MaxVal get_somme_3max(FILE *f) {
  struct MaxVal tabmv[4] = {};

  tabmv[3] = get_somme_groupe(f);
  qsort(tabmv, 4, sizeof(struct MaxVal), cmpmaxval);
  while (tabmv[3].mt != NIL) {
    tabmv[3] = get_somme_groupe(f);
    qsort(tabmv, 4, sizeof(struct MaxVal), cmpmaxval);
  }

  int somme3max = 0;
  for (int i = 0; i < 3; i++)
    somme3max += tabmv[1].max;
  return (struct MaxVal){MAX, somme3max};
}

int main() {
  FILE *f = fopen("input1", "r");

  struct MaxVal mv = get_max(f);
  printf("max %d\n", mv.max);

  fseek(f, 0, SEEK_SET);

  struct MaxVal mv3 = get_somme_3max(f);
  printf("calories %d\n", mv3.max);

  fclose(f);
}
