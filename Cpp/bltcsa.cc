#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <algorithm>
#include <ranges>
#include <functional>

using namespace std;

struct IntLine {
	int intline;
	friend istream &operator>>(istream &s, IntLine &il) {
		string str;
		getline(s, str);
		if (str == "")
			il.intline = -1;
		else
			il.intline = stoi(str);
		return s;
	}
};

int main () {
	ifstream ifin("input1");
	auto intlines = views::istream<IntLine>(ifin)
		| views::transform([](const IntLine &il){ return il.intline; });
	auto sommegr = intlines
		| views::lazy_split( -1 )
		| views::transform( [](const auto &inner){ return ranges::fold_left(inner, 0, plus<int>()); });
	cout << ranges::max(sommegr) << endl;

	ifstream ifin2("input1");
	auto intlines2 = views::istream<IntLine>(ifin2)
		| views::transform([](const IntLine &il){ return il.intline; });
	auto sommegr2 = intlines2
		| views::lazy_split( -1 )
		| views::transform( [](const auto &inner){ return ranges::fold_left(inner, 0, plus<int>()); });

	// il faut G++-14 ou clang-17:  auto t = sommegr2 | ranges::to< std::vector >());
	vector<int> t;
	for(auto const &v: sommegr2)
		t.push_back(v);
		       
	ranges::sort(t, ranges::greater());
	auto max3 = views::take(t, 3);
	for(auto const e: max3)
		cout << " " << e;
	cout << endl;

	
	
	// auto sommegr = maxgr |
	// 	views::transform( [](const auto &inner){ return ranges::fold_left(inner, 0, plus<int>()); });
				   
	// for(auto const &inner: maxgr) {
	// 	auto somme = ranges::fold_left(inner, 0, plus<int>());
	// 	cout << somme << endl;
	// 	// for(const auto elem : inner) {
	// 	// 	cout << elem << endl;
	// 	// }
	// }

	// // OK
	// for(const auto &il : intlines) {
	// 	cout << il << endl;
	// }
	
	// vector<int> vres;
	// for(string line; getline(ifin, line);) {
	// 	istringstream iss(line);
	// 	int somme=0;
	// 	int a=0;
	// 	if ( !(iss >> a)) {
	// 		vres.push_back(somme);
	// 	}
	// 	else {
	// 		somme += a;
	// 	}
	// }
	// auto res = ranges::max(ranges::begin(vres), ranges::end(vres));
	// cout << res << endl;
}
