# BestLanguagesForTeachingCSAlgorithms

## Description

After numerous decades of discussions about the best language to teach
computer science at Ensimag, for incoming students.

Previous languages are:
- Scheme (1980 - 2000)
- Ada (~ 2000)
- Python with itertools (~ 2015)
- Python without itertools (~ 2020)

Next contender may be Go and Rust (no joking, for the students with
strong CS background).

Note: I m not interrested in parallel implementation yet, as go,
rust, c++, c, d, fortran would do the job nicely.

## Problem 1 to implement, from Advent of Code 2022, data science flow

- Read a file named "input1"
- with 1 integer per line or a blank line
- consecutive lines without blank line form a group
- first, sum each group and display the value of the maximum group
- second, sum each group and display the sum of the top three groups

### Reference implémentation: Rust version from Frédéric Wagner
Short, efficient.
https://github.com/wagnerf42/aoc2022/tree/main/day1

## Expected comparison criteria
- clarity of the program
- clarity of the concept used
- memory efficiency (temporary copies)
- performance efficiency (execution time)
- number of lines

## Problem 2 to implement, from The One Billion Row Challenge, data science flow
Read and aggregate 1 billion lines of weather stations temperature
easurement.  The challenge used a purely random uniformly one
generated (only the names seems correct), temperature between -99 and
+99.

Examples from the original challenge website

Input
> Hamburg;12.0
> Bulawayo;8.9
> Palembang;38.8
> St. John's;15.2
> Cracow;12.6
> Bridgetown;26.9
> Istanbul;6.2
> Roseau;34.4
> Conakry;31.2
> Istanbul;23.0

Output (min/mean/max)
> {Abha=-23.0/18.0/59.2, Abidjan=-16.2/26.0/67.3, Abéché=-10.0/29.4/69.0, Accra=-10.1/26.4/66.4, Addis Ababa=-23.7/16.0/67.0, Adelaide=-27.8/17.3/58.5, ...}

### Possible adaptation
- Generate the file on the fly and use pipe ?
- Compress the file and read it without expanding (do not allow easy parallel reading)
- 100K, 1M, 1B (from orignal challenge, several leaderboard)

### Référence implémentation and résults in Java
https://github.com/gunnarmorling/1brc

## Installation

Any standard Linux distributions should provide the right tools
(compilers, interpreters).

For each language, you may use some libraries, if they do not change the
feeling of the languages, as the goal is the comparison between the
languages.

- Rust (Reference implementation from Frédéric Wagner), using itertools
- C (C-2x)
- C++ (C++-2x)
- Objective-C
- D
- Ada
- Go
- Raku
- Crystal
- Scheme (Guile)
- Fortran


## Usage
```bash
make
```

## Support
None.

## Roadmap
- Include parallel alternatives (eg. OpenMP)


## Contributing
Implement in other languages.

## Authors and acknowledgment
- Rust: Frédéric Wagner
- 

## License
The Rust version is the full property of Frédéric Wagner
https://github.com/wagnerf42/aoc2022

All other codes are GPLv3

## Project status
- Start: March 2024
- End: ongoing

