import std.stdio;
import std.algorithm;
import std.array;
import std.conv;
import std.range;

int main() {
  auto file = File("input1");
  auto max = file.byLine()
    .map!(a => (a == "")?"-1":a)
    .map!(a => to!int(a))
    .array()
    .splitter(-1)
    .map!(a => a.sum)
    .fold!((a,b) => (a<b)?b:a);
  writeln(max);	 
    
  // j'aime aussi bien le découpage en deux: la deuxième partie est alors
  // int max=0;
  // foreach(subr; splitter(r.array(), -1)) {
  //   const val = subr.sum();
  //   if (val > max)
  //     max = val;
  // }

  auto file2 = File("input1");
  auto somme3max = file2.byLine()
    .map!(a => (a == "")?"-1":a)
    .map!(a => to!int(a))
    .array()
    .splitter(-1)
    .map!(a => a.sum)
    .array()
    .sort!((a,b) => a>b)
    .array()
    .take(3)
    .sum;

  writeln(somme3max);
  return 0;
}
